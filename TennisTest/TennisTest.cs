﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tennis_kata;

namespace TennisTest {
    [TestClass]
    public class TennisTest {
        private TennisMatch _tennisMatch;

        [TestInitialize]
        public void TestInitialize() {
            _tennisMatch = new TennisMatch {
                Player1 = new Player { Name = "Player1" },
                Player2 = new Player { Name = "Player2" }
            };
        }

        [TestMethod]
        public void Tennis_NoPlayerShouldWin_WhenTheyScoresLessThan4Points() {            
            _tennisMatch.Player1.Score = 1;
            Assert.AreEqual(false, _tennisMatch.AnyoneWins());
            _tennisMatch.Player1.Score = 2;
            Assert.AreEqual(false, _tennisMatch.AnyoneWins());
            _tennisMatch.Player1.Score = 3;
            Assert.AreEqual(false, _tennisMatch.AnyoneWins());
            _tennisMatch.Player1.Score = 4;
            Assert.AreEqual(true, _tennisMatch.AnyoneWins());

            _tennisMatch.Player1.Score = 0;
            _tennisMatch.Player2.Score = 1;
            Assert.AreEqual(false, _tennisMatch.AnyoneWins());
            _tennisMatch.Player2.Score = 2;
            Assert.AreEqual(false, _tennisMatch.AnyoneWins());
            _tennisMatch.Player2.Score = 3;
            Assert.AreEqual(false, _tennisMatch.AnyoneWins());
            _tennisMatch.Player2.Score = 4;
            Assert.AreEqual(true, _tennisMatch.AnyoneWins());
        }

        [TestMethod]
        public void Tennis_PlayerShouldWin_WhenTheScoreIsMinimum4AndTheDifferenceIsMinimum2() {            
            _tennisMatch.Player1.Score = 4;            
            _tennisMatch.Player2.Score = 0;
            Assert.AreEqual(true, _tennisMatch.AnyoneWins());

            _tennisMatch.Player1.Score = 0;
            _tennisMatch.Player2.Score = 4;
            Assert.AreEqual(true, _tennisMatch.AnyoneWins());

            _tennisMatch.Player1.Score = 4;
            _tennisMatch.Player2.Score = 2;
            Assert.AreEqual(true, _tennisMatch.AnyoneWins());

            _tennisMatch.Player1.Score = 4;
            _tennisMatch.Player2.Score = 3;
            Assert.AreEqual(false, _tennisMatch.AnyoneWins());

            _tennisMatch.Player1.Score = 5;
            _tennisMatch.Player2.Score = 3;
            Assert.AreEqual(true, _tennisMatch.AnyoneWins());            
        }

        [TestMethod]
        public void Tennis_ItShouldPrintTheRunningScoreName() {            
            Assert.AreEqual("love", _tennisMatch.GetRunningScoreName(0));            
            Assert.AreEqual("fifteen", _tennisMatch.GetRunningScoreName(1));            
            Assert.AreEqual("thirty", _tennisMatch.GetRunningScoreName(2));            
            Assert.AreEqual("forty", _tennisMatch.GetRunningScoreName(3));
            Assert.AreEqual("", _tennisMatch.GetRunningScoreName(4));
        }

        [TestMethod]
        public void Tennis_ItShouldPrintTheRunningScoreName_WhenTheMatchIsRunning() {            
            _tennisMatch.Player1.Score = 0; _tennisMatch.Player2.Score = 0;
            Assert.AreEqual("love - love", _tennisMatch.PrintRunningScore());

            _tennisMatch.Player1.Score = 1; _tennisMatch.Player2.Score = 1;
            Assert.AreEqual("fifteen - fifteen", _tennisMatch.PrintRunningScore());

            _tennisMatch.Player1.Score = 2; _tennisMatch.Player2.Score = 2;
            Assert.AreEqual("thirty - thirty", _tennisMatch.PrintRunningScore());

            _tennisMatch.Player1.Score = 3; _tennisMatch.Player2.Score = 3;
            Assert.AreEqual("deuce", _tennisMatch.PrintRunningScore());

            _tennisMatch.Player1.Score = 0; _tennisMatch.Player2.Score = 3;
            Assert.AreEqual("love - forty", _tennisMatch.PrintRunningScore());

            _tennisMatch.Player1.Score = 1; _tennisMatch.Player2.Score = 2;
            Assert.AreEqual("fifteen - thirty", _tennisMatch.PrintRunningScore());

            _tennisMatch.Player1.Score = 4; _tennisMatch.Player2.Score = 0;
            Assert.AreEqual(_tennisMatch.Player1.Name + " wins", _tennisMatch.PrintRunningScore());

            _tennisMatch.Player1.Score = 4; _tennisMatch.Player2.Score = 4;
            Assert.AreEqual("deuce", _tennisMatch.PrintRunningScore());

            _tennisMatch.Player1.Score = 4; _tennisMatch.Player2.Score = 3;
            Assert.AreEqual("advantage " + _tennisMatch.Player1.Name, _tennisMatch.PrintRunningScore());

            _tennisMatch.Player1.Score = 4; _tennisMatch.Player2.Score = 5;
            Assert.AreEqual("advantage " + _tennisMatch.Player2.Name, _tennisMatch.PrintRunningScore());

            _tennisMatch.Player1.Score = 2; _tennisMatch.Player2.Score = 4;
            Assert.AreEqual(_tennisMatch.Player2.Name + " wins", _tennisMatch.PrintRunningScore());
        }
    }
}
