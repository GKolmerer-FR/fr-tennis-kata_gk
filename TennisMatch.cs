﻿using System;

namespace Tennis_kata {        
    public class TennisMatch {
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }

        public bool AnyoneWins() {
            if (Player1.Score < 4 && Player2.Score < 4) {
                return false;
            }
            return Math.Abs(Player1.Score - Player2.Score) >= 2;
        }
        public string GetRunningScoreName(int score) {
            switch (score) {
                case 0:
                    return "love";
                case 1:
                    return "fifteen";
                case 2:
                    return "thirty";
                case 3:
                    return "forty";
                default:
                    return "";
            }
        }
        public string PrintRunningScore() {
            if (AnyoneWins()) return GetNameOfThePlayerWithHigherScore() + " wins";
            if (Player1.Score < 3 || Player2.Score < 3) return GetRunningScoreName(Player1.Score) + " - " + GetRunningScoreName(Player2.Score);
            if (Player1.Score == Player2.Score) return "deuce";
            return "advantage " + GetNameOfThePlayerWithHigherScore();
        }
        private string GetNameOfThePlayerWithHigherScore() {
            return (Player1.Score > Player2.Score ? Player1.Name : Player2.Name);
        }
    }    
}
